package org.example;

import org.example.utils.OutputMode;

import java.io.*;

public class Main {
    public static void main( String[] args ) throws IOException {

        File folder = new File("entries_files");
        FeaturesManagement featuresManagement = new FeaturesManagement(folder);
        int number = featuresManagement.chooseNumberOfFiles();
        featuresManagement.filesSelection(number);
        String outputMode = featuresManagement.chooseOutputMode();
        OutputFileWriter outputFileWriter = OutputMode.createOutputFileWriter(outputMode);
        for(String fileName: featuresManagement.getFilesChosen()) {
            FileParser fileParser = new FileParser("entries_files/" + fileName, outputMode);
            fileParser.parse(outputFileWriter);
        }
        outputFileWriter.close();
    }

}

