package org.example;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class AllCodesWriter extends OutputFileWriter {
    private final PrintWriter writer;

    public AllCodesWriter() throws IOException {
       super();
       String outputFileName = "CODES";
       this.writer = new PrintWriter("output/all_codes/" + outputFileName + ".txt", StandardCharsets.UTF_8);
    }

    @Override
    public void write(OutputCode outputCode) {
        writer.append(outputCode.toString());
        writer.println();
    }

    @Override
    public void close() {
        writer.close();
    }
}
