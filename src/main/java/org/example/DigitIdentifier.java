package org.example;

import org.example.utils.ChecksumCalculation;
import org.example.utils.ChecksumType;

public class DigitIdentifier {

    private OutputCode outputCode;

    public DigitIdentifier() {
        this.outputCode = new OutputCode();
    }

    public OutputCode getOutputCode()  {
        return outputCode;
    }

    public void identify(DigitTable digitTable) {
        if(digitTable.representsANumber()) {
            Integer result = digitTable.toNumber();
            outputCode.append(result.toString());
        } else {
            outputCode.append("?");
        }
    }

    public int calculateChecksum() {
        int sum = 0;
        if(this.outputCode.getChecksumType() != ChecksumType.UNKNOWN) {

            for(int i = 0; i < outputCode.getValues().size() ; i++) {
                sum += Integer.parseInt(outputCode.getValues().get(i))*(ChecksumCalculation.indexPositionHelper.get(i));
            }
            if(sum % ChecksumCalculation.MODULO_VALUE != 0) outputCode.setChecksumType(ChecksumType.ERRORED);
        }
        return sum;
    }

}
