package org.example;

import org.example.utils.EntryDetails;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class FileParser {

    FileReader fileReader;
    BufferedReader bufferedReader;
    private final int fileSize;
    private int currentLineCount;

    FileParser(String filePath, String outputModeValue) throws IOException {
        fileSize = (int) Files.lines(Path.of(filePath)).count();
        File file = new File(filePath);
        fileReader = new FileReader(file);
        bufferedReader = new BufferedReader(fileReader);
        currentLineCount = 0;
    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public void nextLine() throws IOException {
        this.bufferedReader.readLine();
    }

    public boolean endOfFile() {
        return currentLineCount == fileSize / EntryDetails.LINE_HEIGHT;
    }

    public static OutputFileWriter createOutputFileWriter(String outputModeValue) throws IOException {
        return Objects.equals(outputModeValue, "GROUPED_CODES") ? new GroupCodesWriter() : new AllCodesWriter();
    }

    public String parse(OutputFileWriter outputFileWriter) throws IOException {
        String parcedEntry = "";
        while(!endOfFile()) {
            currentLineCount++;
            bufferedReader.mark(100);
            EntryParser entryParser = new EntryParser(bufferedReader, outputFileWriter);
            parcedEntry += entryParser.parse() + "\n";
            nextLine();
        }

        return parcedEntry;
    }
}
