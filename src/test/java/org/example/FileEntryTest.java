package org.example;

import org.example.utils.OutputMode;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class FileEntryTest {

    @Test
    public void isValidEntryTest() throws IOException {

        FileParser fileParser = new FileParser("entries_files/authorized_test.txt",OutputMode.numberedChoices.get(1));
        OutputFileWriter outputFileWriter = OutputMode.createOutputFileWriter(OutputMode.numberedChoices.get(1));

        Assert.assertEquals("457508000 \n",fileParser.parse(outputFileWriter));

    }

}
