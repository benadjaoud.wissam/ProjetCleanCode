package org.example;

import org.example.utils.ChecksumCalculation;
import org.example.utils.OutputMode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class OCRTest {
    DigitIdentifier digitIdentifierFalse = new DigitIdentifier();
    DigitIdentifier digitIdentifierTrue = new DigitIdentifier();
    FileParser fileParserAuthorized;
    FileParser fileParserErrored;
    FileParser fileParserUnknown;
    OutputFileWriter outputFileWriterMixed;
    FileParser fileParser;
    OutputFileWriter outputFileWriterGrouped;
    File folderOutputGrouped;
    File fileEntryAuthorized;
    File fileEntryErrored;
    File fileEntryUnknown;
    File folderEntries;
    FeaturesManagement featuresManagement;


    @Before
    public void setUp() throws IOException {
        digitIdentifierFalse.getOutputCode().append("5");
        digitIdentifierFalse.getOutputCode().append("2");
        digitIdentifierFalse.getOutputCode().append("3");
        digitIdentifierFalse.getOutputCode().append("4");
        digitIdentifierFalse.getOutputCode().append("5");
        digitIdentifierFalse.getOutputCode().append("6");
        digitIdentifierFalse.getOutputCode().append("1");
        digitIdentifierFalse.getOutputCode().append("8");
        digitIdentifierFalse.getOutputCode().append("9");

        digitIdentifierTrue.getOutputCode().append("4");
        digitIdentifierTrue.getOutputCode().append("5");
        digitIdentifierTrue.getOutputCode().append("7");
        digitIdentifierTrue.getOutputCode().append("5");
        digitIdentifierTrue.getOutputCode().append("0");
        digitIdentifierTrue.getOutputCode().append("8");
        digitIdentifierTrue.getOutputCode().append("0");
        digitIdentifierTrue.getOutputCode().append("0");
        digitIdentifierTrue.getOutputCode().append("0");

        fileParserAuthorized = new FileParser("entries_files/authorized_test.txt", OutputMode.numberedChoices.get(1));
        fileParserErrored = new FileParser("entries_files/errored_test.txt", OutputMode.numberedChoices.get(1));
        fileParserUnknown = new FileParser("entries_files/unknown_test.txt", OutputMode.numberedChoices.get(1));
        outputFileWriterMixed = OutputMode.createOutputFileWriter(OutputMode.numberedChoices.get(1));

        fileParser = new FileParser("entries_files/unknown_test.txt", OutputMode.numberedChoices.get(2));
        outputFileWriterGrouped = OutputMode.createOutputFileWriter(OutputMode.numberedChoices.get(2));
        folderOutputGrouped = new File("output/group_codes/");
        fileEntryAuthorized = new File("output/group_codes/AUTHORIZED.txt");
        fileEntryErrored = new File("output/group_codes/AUTHORIZED.txt");
        fileEntryUnknown = new File("output/group_codes/AUTHORIZED.txt");

        folderEntries = new File("entries_files");
        featuresManagement = new FeaturesManagement(folderEntries);
    }

    @Test
    public void isValidChecksumTest() {
        Assert.assertFalse(digitIdentifierFalse.calculateChecksum() % ChecksumCalculation.MODULO_VALUE == 0);
        Assert.assertTrue(digitIdentifierTrue.calculateChecksum() % ChecksumCalculation.MODULO_VALUE == 0);
    }

    @Test
    public void isAuthorizedEntryTest() throws IOException {
        Assert.assertEquals("457508000 \n",fileParserAuthorized.parse(outputFileWriterMixed));
    }

    @Test
    public void isErroredEntryTest() throws IOException {
        Assert.assertEquals("123136789 ERR\n",fileParserErrored.parse(outputFileWriterMixed));
    }

    @Test
    public void isUnknownEntryTest() throws IOException {;
        Assert.assertEquals("1231?6789 ILL\n",fileParserUnknown.parse(outputFileWriterMixed));
    }

    @Test
    public void classificationTest() throws IOException {
        fileParser.parse(outputFileWriterGrouped);

        Assert.assertTrue(Objects.requireNonNull(Arrays.stream(folderOutputGrouped.listFiles()).toList().contains(fileEntryAuthorized)));
        Assert.assertTrue(Objects.requireNonNull(Arrays.stream(folderOutputGrouped.listFiles()).toList().contains(fileEntryErrored)));
        Assert.assertTrue(Objects.requireNonNull(Arrays.stream(folderOutputGrouped.listFiles()).toList().contains(fileEntryUnknown)));
    }

    @Test
    public void controllerTest() {
        Assert.assertEquals(5,featuresManagement.numberOfInputFiles());
    }
}
