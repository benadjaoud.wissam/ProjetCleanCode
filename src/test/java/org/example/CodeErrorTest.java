package org.example;

import org.example.utils.OutputMode;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class CodeErrorTest {

    @Test
    public void isValidCodeERR() throws IOException {

        FileParser fileParser = new FileParser("entries_files/errored_test.txt", OutputMode.numberedChoices.get(1));
        OutputFileWriter outputFileWriter = OutputMode.createOutputFileWriter(OutputMode.numberedChoices.get(1));

        Assert.assertEquals("123136789 ERR\n",fileParser.parse(outputFileWriter));

    }

}
