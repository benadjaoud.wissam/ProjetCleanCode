package org.example;

import org.example.utils.OutputMode;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class CodeIllisibleTest {

    @Test
    public void isCodeIllisibleTest() throws IOException {
        FileParser fileParser = new FileParser("entries_files/unknown_test.txt", OutputMode.numberedChoices.get(1));
        OutputFileWriter outputFileWriter = OutputMode.createOutputFileWriter(OutputMode.numberedChoices.get(1));

        Assert.assertEquals("1231?6789 ILL\n",fileParser.parse(outputFileWriter));
    }
}
