package org.example;

import org.example.utils.OutputMode;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ControlToolTest {

    @Test
    public void controlTest() throws IOException {
        File folder = new File("entries_files");
        FeaturesManagement featuresManagement = new FeaturesManagement(folder);

        Assert.assertEquals(5,featuresManagement.numberOfInputFiles());


    }
}
