package org.example;

import org.example.utils.OutputMode;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class ClassificationTest {

    @Test
    public void testFile() throws IOException {
        FileParser fileParser = new FileParser("entries_files/unknown_test.txt", OutputMode.numberedChoices.get(2));
        OutputFileWriter outputFileWriter = OutputMode.createOutputFileWriter(OutputMode.numberedChoices.get(2));
        fileParser.parse(outputFileWriter);
        File folder = new File("output/group_codes/");
        File fileEntryAuthorized = new File("output/group_codes/AUTHORIZED.txt");
        File fileEntryErrored = new File("output/group_codes/AUTHORIZED.txt");
        File fileEntryUnknown = new File("output/group_codes/AUTHORIZED.txt");

        Assert.assertTrue(Objects.requireNonNull(Arrays.stream(folder.listFiles()).toList().contains(fileEntryAuthorized)));
        Assert.assertTrue(Objects.requireNonNull(Arrays.stream(folder.listFiles()).toList().contains(fileEntryErrored)));
        Assert.assertTrue(Objects.requireNonNull(Arrays.stream(folder.listFiles()).toList().contains(fileEntryUnknown)));

    }
}
